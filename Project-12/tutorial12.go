package main

import "fmt"

func main() {
	arr := [3]int{4, 8, 2}
	sum := 0

	arr2D := [2][3]int{{1, 2, 3}, {4, 5, 6}}
	fmt.Println(arr2D[0][2])

	for i := 0; i < len(arr); i++ {
		sum += arr[i]
	}

	fmt.Println(sum)
	/*var arr [5]int
	arr[0] = 100
	arr[4] = 900
	arr[2] = 300
	fmt.Println(arr)*/
}
