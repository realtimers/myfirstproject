package main

import "fmt"

func main() {
	scanner := bufio.NewScanner(os.Stdin)
	fmt.Printf("Type a number: ")
	scanner.Scan()
	ans, _ := strconv.ParseInt(scanner.Text(), 10, 64)

	ans := 10

	switch ans {
	case 1:
		fmt.Println("It is equal to one")
	case 2:
		fmt.Println("It is equal to two")
	case 3:
		fmt.Println("It is equal to three")
	case 4:
		fmt.Println("It is equal to four")
	case 5:
		fmt.Println("It is equal to five")
	case 6:
		fmt.Println("It is equal to six")
	case 7:
		fmt.Println("It is equal to seven")
	case 8:
		fmt.Println("It is equal to eight")
	case 9:
		fmt.Println("It is equal to nine")
	case 10:
		fmt.Println("It is equal to ten")
	default:
		fmt.Println("It is not equal to a number between 1 and 10")

	}
}
