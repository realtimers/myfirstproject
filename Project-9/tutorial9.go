package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
)

func main() {

	scanner := bufio.NewScanner(os.Stdin)
	fmt.Printf("Type your age: ")
	scanner.Scan()
	age, _ := strconv.ParseInt(scanner.Text(), 10, 64)

	if age >= 18 {
		fmt.Println("You are allowed to enter the ride")
	} else if age >= 14 {
		fmt.Println("You are allowed to enter the ride with a parent")
	} else {
		fmt.Println("You are not allowed to ride")
	}

	/*
		scanner := bufio.NewScanner(os.Stdin)
		fmt.Printf("Type your age: ")
		scanner.Scan()
		age, _ := strconv.ParseInt(scanner.Text(), 10, 64)

		if age == 18 || (age > 18) {
			fmt.Println("You are allowed to enter the ride")
		}
		if age >= 14 && age < 18 {
			fmt.Println("You are allowed to enter the ride with a parent")
		}

		if age != 18 && (age < 18) {
			fmt.Println("You are not allowed to enter the ride")
		}
	*/

}
